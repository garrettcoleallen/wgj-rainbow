extends "res://enemy.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal summon
signal death

onready var bullet_scene = preload("res://bullet.tscn")

export var close_gap_distance = 450
export var walk_speed = 40.0

var can_walk = true
var can_be_damaged = true

var rng = RandomNumberGenerator.new()

# Called when the node enters the scene tree for the first time.
var player_node
func _ready():
	player_node = get_tree().get_nodes_in_group("player").pop_front()
	$HealthBar.max_value = max_health
	print(max_health)
	print(health)

func _process(delta):
	if !seeking_player or dead:
		return
	$HealthBar.value = health

func _physics_process(delta):
	if !seeking_player or dead:
		return
	
	var distance_to_player = global_position.distance_to(player_node.global_position)
	if abs(distance_to_player) > close_gap_distance and can_walk:
		if player_node.global_position.x < global_position.x:
			move_and_slide(Vector2(-walk_speed, 0))
		else:
			move_and_slide(Vector2(walk_speed, 0))
			
	#handle flip other way
	if player_node.global_position.x > global_position.x:
		$AlexJones.scale.x = -3
	else:
		$AlexJones.scale.x = 3

func show_healthbar():
	$HealthBar.visible = true

func take_damage(dmg):
	if can_be_damaged:
		.take_damage(dmg)
		
func die():
	$HealthBar.visible = false
	dead = true
	Game.add_score(points)
	stop_all_timers()
	$AnimationPlayer.play("death")
	$EndGameTimer.start()

func stop_all_timers():
	$GunFireRate.stop()
	$GunPhaseTimer.stop()
	$SummonPhaseSpawner.stop()
	$SummonPhaseTimer.stop()
	$PhaseCoolDown.stop()
	$ShieldPhaseTimer.stop()

func fire_gun_at_player():
	var bullet = bullet_scene.instance()
	bullet.global_position = $AlexJones/BulletSpawn.global_position
	bullet.movement_vector = $AlexJones/BulletSpawn.global_position.direction_to(player_node.global_position)
	bullet.speed = 20
	bullet.damage = 5
	get_tree().root.add_child(bullet)
	

func start_next_phase():
	var next_phase = rng.randi_range(0,3)
	print(next_phase)
	match next_phase:
		0, 3:
			print("gun phase")
			gun_phase()
		1:
			print("summon phase")
			summon_phase()
		2:
			print("shield phase")
			shield_phase()
		#3:
			#print("rage short burst move quick")

func shield_phase():
	can_walk = false
	can_be_damaged = false
	$ShieldPhaseTimer.start()
	$AnimationPlayer.play("shield")
	

func summon_phase():
	can_walk = false
	$SummonPhaseTimer.start()
	$AnimationPlayer.play("summon")
	$SummonPhaseSpawner.start()

func gun_phase():
	$AnimationPlayer.play("walk")
	$GunPhaseTimer.start()
	$GunFireRate.start()
	
func phase_cooldown():
	can_walk = true
	$AnimationPlayer.play("walk")
	$PhaseCoolDown.start()
	$GunFireRate.stop()
	$GunPhaseTimer.stop()
	

func _on_PhaseCoolDown_timeout():
	start_next_phase()


func _on_GunPhaseTimer_timeout():
	phase_cooldown()


func _on_GunFireRate_timeout():
	fire_gun_at_player()


func _on_SummonPhaseSpawner_timeout():
	emit_signal("summon")


func _on_SummonPhaseTimer_timeout():
	phase_cooldown()


func _on_ShieldPhaseTimer_timeout():
	can_walk = true
	can_be_damaged = true
	phase_cooldown()


func _on_EndGameTimer_timeout():
	emit_signal("death")
