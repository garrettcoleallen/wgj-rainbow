extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var frog_scene = preload("res://Frog.tscn")
onready var frog_gun_scene = preload("res://Frog-gun.tscn")

enum QuoteAuthor {
	AJ,
	Player
}

var boss_fight_started = false
var dialog_initiated = false

var dialogs = [
	{ dialog = "They told me I was crazy, \n\nbut to me that matters not...", author = QuoteAuthor.AJ },
	{ dialog = "I am simply an ambassador of truth,\n\nand you lead me directly to it...", author = QuoteAuthor.AJ },
	{ dialog = "For that I must show you gratitude...\n\nPlease, turn back, I'll take it from here.", author = QuoteAuthor.AJ },
	{ dialog = "End this folly now for you are no match,\n\nI have predicted your every move...", author = QuoteAuthor.AJ },
	{ dialog = "and set this final stage for your demise...", author = QuoteAuthor.AJ },
	{ dialog = "...", author = QuoteAuthor.Player },
	{ dialog = "Of course I understand that you must see this through,\n\nI want you to know that your bravery is admirable...", author=QuoteAuthor.AJ },
	{ dialog = "But it matters not...", author=QuoteAuthor.AJ },
	{ dialog = "The outcome has already been determined.\n\nIt's been etched into the stars since beginningless time...", author=QuoteAuthor.AJ },
	{ dialog = "Bear witness now.\n\nThe trajectories of our destinies are about to intersect...", author=QuoteAuthor.AJ },
	{ dialog = "There is only one path forward...", author=QuoteAuthor.AJ },
	{ dialog = "Only one of us will emerge...", author=QuoteAuthor.AJ },
	{ dialog = "And reshape the universe with our bare hands...", author=QuoteAuthor.AJ }
]

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if (Input.is_action_just_released("ui_accept") or Input.is_action_just_released("fire")) and !boss_fight_started and dialog_initiated:
		play_next_dialog()



func play_next_dialog():
	var next_dialog = dialogs.pop_front()
	var player = get_tree().get_nodes_in_group("player").pop_front()
	if !next_dialog:
		boss_fight_started = true
		$BossEntranceTrigger/CollisionShape2D.disabled = true
		player.set_process(true)
		player.set_physics_process(true)
		$DialogCamera.current = false
		player.set_camera_enabled()
		$AJDialog.visible = false
		get_player_dialog_box().visible = false
		$AJ.seeking_player = true
		$AJ.gun_phase()
		$AJ.show_healthbar()
		
		
	else:
		if next_dialog.author == QuoteAuthor.AJ:
			get_player_dialog_box().visible = false
			$AJDialog.visible = true
			$AJDialog/Dialog.text = next_dialog.dialog
			$DialogCamera.global_position = $AJ.global_position
			
		else:
			$DialogCamera.global_position = player.global_position
			get_player_dialog_box().visible = true
			$AJDialog.visible = false
	

func get_player_dialog_box():
	return get_tree().get_nodes_in_group("player-dialog").pop_front()

func _on_BossEntranceTrigger_body_entered(body):
	if body.is_in_group("player"):
		dialog_initiated = true
		$bossdoor.global_position.y = 444
		body.set_process(false)
		body.set_physics_process(false)
		play_next_dialog()
		$DialogCamera.current = true


func _on_AJ_summon():
	var frog1 = frog_scene.instance()
	var frog2 = frog_scene.instance()
	var frog_gun1 = frog_gun_scene.instance()
	
	frog1.seeking_player = true
	frog2.seeking_player = true
	frog_gun1.seeking_player = true
	
	frog1.global_position = $EnemySpawnLocations/FrogSpawn1.global_position
	frog2.global_position = $EnemySpawnLocations/FrogSpawn2.global_position
	frog_gun1.global_position = $EnemySpawnLocations/FrogGunSpawn1.global_position
	get_tree().root.add_child(frog1)
	get_tree().root.add_child(frog2)
	get_tree().root.add_child(frog_gun1)

