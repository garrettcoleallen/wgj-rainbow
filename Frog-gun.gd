extends "res://enemy.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var bullet_scene = preload("res://bullet.tscn")

const JUMP_POWER = 200
const MAX_SPEED = 600

var player_node = null
var velocity = Vector2(0,0)


# Called when the node enters the scene tree for the first time.
func _ready():
	player_node = find_player_node()
	$changes/bow.visible = false
	$changes/flag.visible = false
	
func _physics_process(delta):
	if !seeking_player:
		if global_position.distance_to(player_node.global_position) < activation_distance:
			seeking_player = true
		return
	var vector_to_player = global_position.direction_to(player_node.global_position)
	
	if global_position.distance_to(player_node.global_position) < 550 and is_on_floor() and $AttackTimer.is_stopped():
		velocity = Vector2(0,0)
		$frog.frame = 2
		$FrogTongue.visible = true
		$gun.visible = true
		$gun.look_at(player_node.global_position)
		if global_position.x > player_node.global_position.x:
			$gun.scale.x = -2
			$gun.rotate(PI)
		else:
			$gun.scale.x = 2
			
		if $AimTimer.is_stopped():
			$AimTimer.start()
	elif $AimTimer.is_stopped():
		$FrogTongue.visible = false
		$gun.visible = false
		if is_on_floor():
			$frog.frame = 0
			velocity = Vector2(0,0)
			if $JumpTimer.is_stopped():
				velocity += (vector_to_player.normalized() + Vector2(0, -4)) * JUMP_POWER
				$JumpTimer.start()
		else:
			$frog.frame = 1
		
	
	velocity.y += GRAVITY
	
	if velocity.length() >= MAX_SPEED:
		velocity = velocity.normalized() * MAX_SPEED
	move_and_slide(velocity, Vector2(0, -1))
	
	
		
	#handle_tongue(delta)
	
func take_damage(dmg):
	.take_damage(dmg)
	if health < max_health - max_health/4:
		$changes/bow.visible = true
	if health < max_health/4:
		$changes/flag.visible = true
	
#
#func handle_tongue(delta):
#	if is_attacking:
#		if $tongue.scale.x < attack_distance:
#			$tongue.scale.x += 1500 * delta
#			$TongueProjectile.global_position.x = global_position.x + ($tongue.scale.x * cos($tongue.rotation))
#			$TongueProjectile.global_position.y = global_position.y + ($tongue.scale.x * sin($tongue.rotation))
#		else:
#			is_attacking = false
#			attack_retract = true
#	elif attack_retract:
#		$TongueProjectile.monitoring = false
#		$tongue.scale.x -= 1200 * delta
#		if $tongue.scale.x <= 1:
#			$tongue.visible = false
#			attack_retract = false
	
func try_attack():
	$AttackTimer.start()
	var bullet = bullet_scene.instance()
	bullet.global_position = $gun/Position2D.global_position
	#new_round.active_colors = ['r', 'o', 'y', 'g', 'b', 'i', 'v']
	bullet.movement_vector = $gun/Position2D.global_position.direction_to(player_node.global_position)
	get_tree().root.add_child(bullet)
		


func _on_AimTimer_timeout():
	try_attack()
