extends "res://enemy.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

const JUMP_POWER = 300
const MAX_SPEED = 750

var player_node = null
var velocity = Vector2(0,0)

var is_attacking = false
var attack_retract = false
var attack_distance = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	player_node = find_player_node()
	$changes/bow.visible = false
	$changes/flag.visible = false
	
func _physics_process(delta):
	if !seeking_player:
		if global_position.distance_to(player_node.global_position) < activation_distance:
			seeking_player = true
		return
	var vector_to_player = global_position.direction_to(player_node.global_position)
	
	if is_on_floor():
		$frog.frame = 0
		velocity = Vector2(0,0)
		if $JumpTimer.is_stopped() and !is_attacking:
			velocity += (vector_to_player.normalized() + Vector2(0, -4)) * JUMP_POWER
			$JumpTimer.start()
	else:
		$frog.frame = 1
		
	
	velocity.y += GRAVITY
	
	if velocity.length() >= MAX_SPEED:
		velocity = velocity.normalized() * MAX_SPEED
	move_and_slide(velocity, Vector2(0, -1))
	
	if global_position.distance_to(player_node.global_position) < 450 and is_on_floor():
		try_attack()
		
	handle_tongue(delta)
	
func take_damage(dmg):
	.take_damage(dmg)
	if health < max_health - max_health/4:
		$changes/bow.visible = true
	if health < max_health/4:
		$changes/flag.visible = true
	
	
func handle_tongue(delta):
	if is_attacking:
		if $tongue.scale.x < attack_distance:
			$tongue.scale.x += 1500 * delta
			$TongueProjectile.global_position.x = global_position.x + ($tongue.scale.x * cos($tongue.rotation))
			$TongueProjectile.global_position.y = global_position.y + ($tongue.scale.x * sin($tongue.rotation))
		else:
			is_attacking = false
			attack_retract = true
	elif attack_retract:
		$TongueProjectile.monitoring = false
		$tongue.scale.x -= 1200 * delta
		if $tongue.scale.x <= 1:
			$tongue.visible = false
			attack_retract = false
	
func try_attack():
	if $AttackTimer.is_stopped():
		$tongue.look_at(player_node.global_position)
		$AttackTimer.start()
		$tongue.visible = true	
		$tongue.scale.x = 1
		is_attacking = true
		attack_distance = $tongue.global_position.distance_to(player_node.global_position) + 30
		$TongueProjectile.monitoring = true
		
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_tongueprojectile_body_entered(body):
	if body.is_in_group("player"):
		body.take_damage(15)
		attack_retract = true
		is_attacking = false
