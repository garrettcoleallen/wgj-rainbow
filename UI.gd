extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$UINode/HPLabel/HPValue.text = str(get_tree().get_nodes_in_group("player").pop_front().health)
	$UINode/ScoreLabel/ScoreValue.text = str(Game.score)


func _on_Player_hurt():
	$hurtscreen/hurtplayer.play("hurt")


func _on_Player_die():
	$gameoverpanel.visible = true
	$UINode/HPLabel.visible = false
	$gameoverpanel/gameoverlabel.visible = true
	$gameoverpanel/gamewinlabel.visible = false
	$gameoverpanel/finalscore/finalscorevalue.text = str(Game.score)


func _on_AJ_death():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	var player_node = get_tree().get_nodes_in_group("player").pop_front()
	player_node.game_over()
	player_node.set_process(false)
	player_node.set_physics_process(false)
	$gameoverpanel.visible = true
	$gameoverpanel/gameoverlabel.visible = false
	$gameoverpanel/gamewinlabel.visible = true
	$gameoverpanel/finalscore/finalscorevalue.text = str(Game.score + player_node.health)
