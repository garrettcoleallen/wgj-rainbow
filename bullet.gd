extends Node2D


export var movement_vector : Vector2 = Vector2()

var damage = 20
var speed = 40
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	#rotation = atan2(movement_vector.y, movement_vector.x) / (2*PI)
	#print(rotation)

func _physics_process(delta):
	position = position + (movement_vector.normalized() * speed)

func _on_Area2D_body_entered(body):
	if body.has_method("take_damage"):
		body.take_damage(damage)
	queue_free()
		


func _on_Timer_timeout():
	queue_free()

