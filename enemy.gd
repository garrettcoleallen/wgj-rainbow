extends KinematicBody2D


# this should be a singleton
const GRAVITY = 20

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var activation_distance = 700 # how close player gets before toggle on seek
export var seeking_player = false # should seek player or not yet

export var max_health = 20.0

var health = 20.0

export var points = 10

var dead = false


# Called when the node enters the scene tree for the first time.
func _ready():
	health = max_health
	pass # Replace with function body.

func take_damage(dmg):
	if dead or !seeking_player:
		return
	health -= dmg
	if health <= 0:
		die()
		
func die():
	dead = true
	queue_free()
	Game.add_score(points)

func find_player_node():
	return get_tree().get_nodes_in_group("player").pop_front()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
