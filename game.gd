extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var score = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	reset_game()


func reset_game():
	score = 0
	
	
func add_score(points):
	score += points
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
