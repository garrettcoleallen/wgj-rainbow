extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal hurt
signal die

const ACCEL = 120
const DECEL = 60
const GRAVITY = 20
const JUMP_ACCEL = 700

const MAX_RUN_SPEED = 400
const MAX_FALL_SPEED = 700
# const AIM_ROTATE_RATE = 2

const MAX_HEALTH = 100.0

var game_over = false

onready var round_scene = preload("res://rainbow_round.tscn")
#josh is a big gay
var movement_vector : Vector2 = Vector2(0,0)
var double_jump : bool = true

var health : float = MAX_HEALTH
var dead = false


var camera_default_position
# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	camera_default_position = $Camera2D.position

func game_over():
	game_over = true
	

func take_damage(dmg):
	if dead or game_over:
		return
		
	emit_signal("hurt")
	health -= dmg
	if health <= 0:
		die()
		
func die():
	dead = true
	$"death effects".visible = true
	$AnimationPlayer.play("death")
	$gun.visible = false
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	set_process(false)
	set_physics_process(false)
	emit_signal("die")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$crosshair.global_position = $Camera2D.get_global_mouse_position()
	var max_camera_distance = 120
	var camera_aim_direction = global_position.direction_to($Camera2D.get_global_mouse_position())
	var camera_aim_distance = global_position.distance_to($Camera2D.get_global_mouse_position())
	
	if camera_aim_distance > max_camera_distance:
		camera_aim_distance = max_camera_distance
	
	$Camera2D.position = camera_default_position + camera_aim_direction * camera_aim_distance
	

func _physics_process(delta):
	player_movement(delta)
	gun_aim(delta)
	gun_fire(delta)
	
func gun_fire(delta):
	if Input.is_action_pressed("fire"):
		if $ShootingTimer.is_stopped():
			var new_round = round_scene.instance()
			new_round.global_position = $gun/MuzzlePosition.global_position
			#new_round.active_colors = ['r', 'o', 'y', 'g', 'b', 'i', 'v']
			new_round.active_colors = ['r']
			new_round.movement_vector = $gun/MuzzlePosition.global_position.direction_to($Camera2D.get_global_mouse_position())
			new_round.rotation = $gun.rotation
			get_tree().root.add_child(new_round)
			$ShootingTimer.start()
		
	
func gun_aim(delta):
	
	
	var mouse_pos = $Camera2D.get_global_mouse_position()
	$gun.look_at(mouse_pos)
# THIS WILL WORK BUT IT DOESNT ACCOUNT FOR 180 FLIP SO IDK
#	print($gun.rotation)
#	print("modulo", fmod($gun.rotation, (2 * PI)))
#	$gun.rotation = fmod($gun.rotation, (2 * PI))
#	var rotate_amount = delta * AIM_ROTATE_RATE
#	var desired_location_angle = $gun.get_angle_to(mouse_pos)
#
#	if abs(desired_location_angle) < rotate_amount:
#		$gun.rotation += desired_location_angle
#	else:
#		print("will rotate by ", rotate_amount)
#		if desired_location_angle < 0:
#			$gun.rotate(-rotate_amount)
#		else:
#			$gun.rotate(rotate_amount)

	
	if mouse_pos.x < global_position.x:
		$gun.scale.x = abs($gun.scale.x)*-1
		$gun.position.x = abs($gun.position.x)
		$gun.rotation_degrees += 180 #correct for flip
		$body.scale.x = abs($body.scale.x)*-1
	else:
		$gun.scale.x = abs($gun.scale.x)
		$gun.position.x = -1*abs($gun.position.x)
		$body.scale.x = abs($body.scale.x)
		
func player_movement(delta):
	
	var left_right = 0
	var run_accel_modifier = 1
	
	if Input.is_action_pressed("ui_right"):
		left_right = 1
	elif Input.is_action_pressed("ui_left"):
		left_right = -1
		
	if is_on_floor():
		# reset double jump on ground
		if not double_jump:
			double_jump = true
	else:
		run_accel_modifier = 0.4
			
	# no left or right pressed so decel
	if abs(left_right) == 0:
		if abs(movement_vector.x) < DECEL:
			movement_vector.x = 0
		else: 
			movement_vector.x -= (movement_vector.x / abs(movement_vector.x)) * DECEL
		
	if abs(movement_vector.x) > MAX_RUN_SPEED:
		movement_vector.x = (movement_vector.x / abs(movement_vector.x)) * MAX_RUN_SPEED
	else:
		movement_vector.x += left_right * ACCEL * run_accel_modifier
		
	if (Input.is_action_pressed("ui_up") or Input.is_action_pressed("ui_accept")) and ((is_on_floor() or (Input.is_action_just_pressed("ui_up") and double_jump)) and $JumpTimer.is_stopped()):
		movement_vector.y = -JUMP_ACCEL
		$JumpTimer.start()
		if not is_on_floor():
			double_jump = false
			
	elif !is_on_floor():
		movement_vector.y += GRAVITY
		# only checking on fall so jump can be fast
		if movement_vector.y > MAX_FALL_SPEED:
			movement_vector.y = MAX_FALL_SPEED
	else:
		movement_vector.y = 1 #fuck me
	
	
	move_and_slide(movement_vector, Vector2(0,-1))
	
	if !is_on_floor():
		play_if_not_playing($AnimationPlayer, "jump")
	elif abs(movement_vector.x) > 0:
		play_if_not_playing($AnimationPlayer, "run")
	else:
		play_if_not_playing($AnimationPlayer, "idle")
	
func play_if_not_playing(anim_player: AnimationPlayer, anim: String):
	if anim_player.current_animation != anim:
		anim_player.play(anim)

func set_camera_enabled():
	$Camera2D.current = true



func _on_Button_pressed():
	Game.reset_game()
	get_tree().change_scene("res://game.tscn")
