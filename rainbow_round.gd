extends Node2D


export var movement_vector : Vector2 = Vector2()

export var speed_per_color = 3
export var damage_per_color = 0.1
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var active_colors = []

# Called when the node enters the scene tree for the first time.
func _ready():
	for i in range(0, len(active_colors)):
		var color_node : Sprite = get_node(active_colors[i])
		color_node.visible = true

func _physics_process(delta):
	position = position + (movement_vector.normalized() * 21 * len(active_colors))

func _on_Area2D_body_entered(body):
	if body.has_method("take_damage") and !body.is_in_group("player"):
		body.take_damage(damage_per_color * len(active_colors))
	queue_free()
		


func _on_Timer_timeout():
	queue_free()
